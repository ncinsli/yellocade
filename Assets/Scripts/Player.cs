﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    [Range(0, 10)] public float _speed = 5f; 
    private Transform _playerT;
    private Rigidbody2D _playerRb;
    private Collider2D _playerCol;
    private Animator _playerAnim;
    private SpriteRenderer _playerSprite;
    private IsGrounded _igScript; //Скрипт, который поможет нам понять, стоим ли мы НОГАМИ на платформе, или нет

    // Start is called before the first frame update
    void Start()
    {
        //Ставим изначальное значение HP
        PlayerPrefs.SetInt("HP", 100);

        _playerT = GetComponent<Transform>();
        _playerRb = GetComponent<Rigidbody2D>();
        _playerCol = GetComponent<Collider2D>();
        _playerAnim = GetComponent<Animator>();
        _playerSprite = GetComponent<SpriteRenderer>();
        _igScript = GetComponentInChildren<IsGrounded>(); 
        
        /*
          Т.К сам по себе скрипт IsGrounded- это новый тип компонента, он может быть прикреплен к любому объекту,
          убеждаемся, что мы имеем дело со скриптом именно НИЖНЕГО КОЛЛАЙДЕРА 
        */
    }


    public void Jump()
    {
        if (_igScript._isGrounded)
            _playerRb.velocity = new Vector2(0, 10);
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        Vector2 direction = new Vector2(Input.GetAxis("Horizontal"), 1);
        MoveX(direction);
    }

    public void MoveX(Vector2 direction)
    {

        if (direction.x != 0)
            _playerAnim.SetInteger("AnimId", 1);
        else
            _playerAnim.SetInteger("AnimId", 0);

        if (direction.x > 0){
            _playerT.localScale = new Vector3(4.241f, 4.241f);
            PlayerPrefs.SetInt("rotatedRight", 1);
        }
        else if(direction.x < 0){
            _playerT.localScale = new Vector3(-4.241f, 4.241f);
            PlayerPrefs.SetInt("rotatedRight", -1);
        }

        _playerRb.position = new Vector2(_playerRb.position.x + direction.x * 2f * Time.deltaTime, _playerT.position.y);
       // _playerT.position = new Vector2(_playerT.position.x + direction.x * 2f* Time.deltaTime, _playerT.position.y);

    }



}
