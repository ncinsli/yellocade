﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IsGrounded : MonoBehaviour
{
    public bool _isGrounded;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
       _isGrounded = true;
        
    }

    private void OnCollisionExit2D(Collision2D collision)
    {
       
        _isGrounded = false;
        
    }
}
