﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class AI : MonoBehaviour
{
    //Объекты, связанные с ботом
    [Range(0, 10)] public float _speed = 5f;
    private Transform _aiT;
    private Rigidbody2D _aiRb;
    private Collider2D _aiCol;
    private Animator _aiAnim;
    private SpriteRenderer _aiSprite;

    //Объекты, связанные с игроком
    private Transform _playerT;
    private Rigidbody2D _playerRb;
    private Collider2D _playerCol;
    private SpriteRenderer _playerSprite;

    private IsGrounded _igScript; //Скрипт, который поможет нам понять, стоим ли мы НОГАМИ на платформе, или нет
    private int _generatedDirections;


    void Start()
    {
        _aiT = GetComponent<Transform>();
        _aiRb = GetComponent<Rigidbody2D>();
        _aiCol = GetComponent<Collider2D>();
        _aiAnim = GetComponent<Animator>();
        _aiSprite = GetComponent<SpriteRenderer>();
        _igScript = GetComponentInChildren<IsGrounded>();

        _playerT = GameObject.FindGameObjectWithTag("Player").GetComponent<Transform>();
        _playerRb = GameObject.FindGameObjectWithTag("Player").GetComponent<Rigidbody2D>();
        _playerCol = GameObject.FindGameObjectWithTag("Player").GetComponent<Collider2D>();
        _playerSprite = GameObject.FindGameObjectWithTag("Player").GetComponent<SpriteRenderer>();

        /*Т.К сам по себе скрипт IsGrounded- это новый тип компонента, он может быть прикреплен к любому объекту,
          убеждаемся, что мы имеем дело со скриптом именно НИЖНЕГО КОЛЛАЙДЕРА */
    }


    void FixedUpdate()
    {
        StartCoroutine(MoveX());
    }

    IEnumerator MoveX()
    {
        Vector3 _aiTarget = _playerT.position / generateDirection();
        Vector3 _movementDirection = _aiTarget - _playerT.position;
        int dirX = generateDirection();
        _movementDirection = _movementDirection.normalized;
        _aiAnim.SetInteger("AnimId", 1);
        _aiRb.position += (Vector2)_movementDirection;
        yield return new WaitForSeconds(1f);

    }


    int generateDirection()
    {
        float inpx = 1;
        _generatedDirections++;
        if(_generatedDirections%6==0)
            inpx = Random.Range(-1f, 1f);

        int output = 0;
        if (inpx < 0) output = -1;
        else output = 1;
        return output;
    }
}
