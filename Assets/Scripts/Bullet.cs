﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{

    public Transform gunTransform;
    private int _direction;

    // Start is called before the first frame update
    void Start()
    {
        _direction = PlayerPrefs.GetInt("rotatedRight");


    }

    // Update is called once per frame
    void Update()
    {
        GetComponent<Rigidbody2D>().position += new Vector2(transform.right.x*0.1f * _direction,transform.right.y);
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Bot"))
            Destroy(collision.gameObject);
        Destroy(gameObject);
    }
}
