﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening; //DOTween

public class yhopperSystem : MonoBehaviour
{
    [Header("Decoration")]
    [SerializeField] private Transform _player; //Для того, чтобы ловушка поднималась с игроком
    [SerializeField] private Transform _point1;
    [SerializeField] private Transform _point2;
    [Header("Shooting")]
    [SerializeField] private float _speed;
    [SerializeField] private GameObject _damageBullet;
    [SerializeField] [Range(0.1f, 3)] float _density;
    [Header("Getting Damage")]
    [SerializeField] private Collider2D _bulletByPlayer; //Пулька, которую испускает наш игрок
    [Header("Showing Damage")]
    public Sprite[] _damagingTextures = new Sprite[3];

    //Служебные компоненты:
    private SpriteRenderer _sprite;
    private Collider2D _col;
    /*  Наш yhopper двигается вверх-вниз по точкам,
        в дальнейшем это пригодится, если 
        надо будет создать горизонтальную, либо диагональную пушку */

    void Start()
    {
        StartCoroutine(TurretShoot(_density));
        PlayerPrefs.SetInt("YHP", 20);
        _sprite = GetComponent<SpriteRenderer>();
        _col = GetComponent<Collider2D>();
    }

 


    private IEnumerator TurretShoot(float density) //Density- плотность
    {

        while (true){
            Vector2 _direction = _player.position - transform.position;
            RaycastHit2D ray = Physics2D.Raycast(transform.position, _direction);
            Debug.DrawRay(transform.position, _direction);
            if (ray.collider.tag == "Player"){
                GameObject _newBullet=Instantiate(_damageBullet);
                _newBullet.transform.position = new Vector2(transform.position.x+0.1f, transform.position.y);
                transform.DOMove(_point1.position, _speed);
                transform.DOMove(_point2.position, _speed);
            }
            yield return new WaitForSeconds(density);
        }
    }
    
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("PlayerBullet"))
            PlayerPrefs.SetInt("YHP", PlayerPrefs.GetInt("YHP") - 1);
        
        int _playerHP = PlayerPrefs.GetInt("YHP");

        
        if (_playerHP < 19) _sprite.sprite = _damagingTextures[0];
        if (_playerHP < 14) _sprite.sprite = _damagingTextures[1];
        if (_playerHP < 8) _sprite.sprite = _damagingTextures[2];
        if (_playerHP < 1)
        {
            _col.enabled = false; //Выключаем коллайдер, так как в момент растворения турель уже уничтожена
            _sprite.DOFade(0f, 1f);
            if(_sprite.color.a<0.5f)
                Destroy(gameObject);
        }
        
        Debug.Log(PlayerPrefs.GetInt("YHP"));
    }
}
