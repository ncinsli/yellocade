﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DamagingBullet : MonoBehaviour
{
    public Transform gunTransform;
    private Rigidbody2D _bulletRb;
    public Transform _player;

    // Start is called before the first frame update
    void Start()
    {
        _bulletRb = GetComponent<Rigidbody2D>();
        _player = GameObject.FindGameObjectWithTag("Player").GetComponent<Transform>();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
       Vector2 _direction = _player.position - gunTransform.position;

       _bulletRb.position += _direction.normalized*Time.deltaTime;
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Player")) {
            PlayerPrefs.SetInt("HP", PlayerPrefs.GetInt("HP") - 1);
            Debug.Log(PlayerPrefs.GetInt("HP"));
            Destroy(gameObject);
        }
        else if(collision.gameObject.tag!="Bullet")
            Destroy(gameObject);

        Debug.Log(collision.gameObject.tag);
    }
}
