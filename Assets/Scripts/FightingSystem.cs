﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FightingSystem : MonoBehaviour
{
    /* 1. Сделать движение пушки
       2. Сделать стрельбу пули */

    Vector2 _mouseDirection;
    public GameObject _bulletOriginal;
    private int _direction;

    void Start()
    {
    }

    void Update()
    {
        _mouseDirection = Camera.main.WorldToScreenPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, Camera.main.nearClipPlane));

        //Крутим нашу пушку по направлению к ней
        Quaternion rot = Quaternion.LookRotation(-_mouseDirection) * Quaternion.Euler(0, 90, 0);
        transform.rotation = rot;

       

    }


    public void OnShooted()
    {
         GameObject _bulletClone = Instantiate(_bulletOriginal);
         
        _bulletClone.transform.position = transform.position;
    }
}
