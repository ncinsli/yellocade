﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour
{
    [Range(0, 1)] [SerializeField] private float _lerp = 0.8f;
    public float _divisionX = 1; //for parallax
    public float _divisionY = 1; //if u want an this. to follow coords near player
    public Transform _player;
    private Transform _camera;
    private Vector3 _playerModded; // A Vector like player.pos, but with configured Z
    public float _z = -5;
    public bool _useY = true;
    private int _y;

    // Start is called before the first frame update
    void Start()
    {
        _camera = GetComponent<Transform>();
        _camera.position = new Vector3(_camera.position.x,_camera.position.y*_y,-5);


        if (_useY) _y = 1;
        else _y = 0;
    }

    // Update is called once per frame
    void Update()
    {
        _playerModded = new Vector3(_player.position.x/_divisionX, _player.position.y*_y/_divisionY, _z);

        transform.position = Vector3.Lerp(_playerModded,_camera.position, _lerp);
    }

    
}
