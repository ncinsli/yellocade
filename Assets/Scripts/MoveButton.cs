﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class MoveButton : MonoBehaviour
{
    [Range(0, 10)] public float _speed = 4f;
    public bool isMoving { get; set; }
    public int direction { get; set; }
    private Transform _playerT;
    private Rigidbody2D _playerRb;
    private Player _playerScript;
    private Animator _playerAnim;
    

    private void Start()
    {
        _playerRb = GameObject.FindGameObjectWithTag("Player").GetComponent<Rigidbody2D>();
        _playerT = GameObject.FindGameObjectWithTag("Player").GetComponent<Transform>();
        _playerScript = GameObject.FindGameObjectWithTag("Player").GetComponent<Player>();
        _playerAnim = GameObject.FindGameObjectWithTag("Player").GetComponent<Animator>();
    }


    private void Update()
    {  
      
        if(isMoving) MoveByButton();
    }

    public void MoveByButton() // -1 is left, 1 is right
    {
       
        if (direction != 0)
            _playerAnim.SetInteger("AnimId", 1);
        else
            _playerAnim.SetInteger("AnimId", 0);

        if (direction > 0)
        {
            _playerT.localScale = new Vector3(4.241f, 4.241f);
            PlayerPrefs.SetInt("rotatedRight", 1);
        }
        else if (direction < 0)
        {
            _playerT.localScale = new Vector3(-4.241f, 4.241f);
            PlayerPrefs.SetInt("rotatedRight", -1);
        }

        Vector2 directionX = new Vector2(direction, 0);
        //_playerRb.position = new Vector2(_playerRb.position.x + direction * Time.deltaTime * _speed, _playerT.position.y);
            
        _playerScript.MoveX(directionX);

    }

}




